# Squeezenet on Cifar-10 Implemented by Pytorch

## About Squeezenet

- ### 压缩策略
  - 将 ![[公式]](https://www.zhihu.com/equation?tex=3%5Ctimes3) 卷积替换成 ![[公式]](https://www.zhihu.com/equation?tex=1%5Ctimes1) 卷积：通过这一步，一个卷积操作的参数数量减少了9倍；
  - 减少 ![[公式]](https://www.zhihu.com/equation?tex=3%5Ctimes3) 卷积的通道数：一个 ![[公式]](https://www.zhihu.com/equation?tex=3%5Ctimes3) 卷积的计算量是 ![[公式]](https://www.zhihu.com/equation?tex=3%5Ctimes3%5Ctimes+M+%5Ctimes+N) （其中 ![[公式]](https://www.zhihu.com/equation?tex=M) ， ![[公式]](https://www.zhihu.com/equation?tex=N) 分别是输入Feature Map和输出Feature Map的通道数），作者任务这样一个计算量过于庞大，因此希望将 ![[公式]](https://www.zhihu.com/equation?tex=M) ， ![[公式]](https://www.zhihu.com/equation?tex=N) 减小以减少参数数量；
  - 将降采样后置：作者认为较大的Feature Map含有更多的信息，因此将降采样往分类层移动。注意这样的操作虽然会提升网络的精度，但是它有一个非常严重的缺点：即会增加网络的计算量。

- ### Fire模块

![](https://pic3.zhimg.com/v2-5fde12f060519e493cb059484514f88a_r.jpg)

- ### 整体架构

![](https://pic3.zhimg.com/80/v2-5f8ff8cb94babde05e69365336e77a62_720w.jpg)

## Result

|    model    | Accuracy |
| :---------: | -------- |
| Squeezenet1 | 90.3%    |
| Squeezenet2 | 90.8%    |
| Squeezenet3 | 90.3%    |

## Reference links

- https://zhuanlan.zhihu.com/p/49465950
