import torch
import torch.nn as nn

__all__ = ['squeezenet1', 'squeezenet2', 'squeezenet3']


class Fire(nn.Module):

    def __init__(self, in_channel, out_channel, squzee_channel):

        super().__init__()
        self.squeeze = nn.Sequential(
            nn.Conv2d(in_channel, squzee_channel, 1),
            nn.BatchNorm2d(squzee_channel, momentum=0.9),
            nn.CELU(inplace=True)) 
        
        self.expand_1x1 = nn.Sequential(
            nn.Conv2d(squzee_channel, int(out_channel / 2), 1),
            nn.BatchNorm2d(int(out_channel / 2), momentum=0.9),
            nn.CELU(inplace=True)
        )

        self.expand_3x3 = nn.Sequential(
            nn.Conv2d(squzee_channel, int(out_channel / 2), 3, padding=1),
            nn.BatchNorm2d(int(out_channel / 2), momentum=0.9),
            nn.CELU(inplace=True)
        )
    
    def forward(self, x):

        x = self.squeeze(x)
        x = torch.cat([
            self.expand_1x1(x),
            self.expand_3x3(x)
        ], 1)

        return x

class SqueezeNet(nn.Module):

    """mobile net with simple bypass"""
    def __init__(self, class_num=10, version=2.0):

        super().__init__()
        self.stem = nn.Sequential(
            nn.Conv2d(3, 96, 3, padding=1),
            nn.BatchNorm2d(96),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(2, 2)
        )
        self.version = version

        self.fire2 = Fire(96, 128, 16)
        self.fire3 = Fire(128, 128, 16)
        self.fire4 = Fire(128, 256, 32)
        self.fire5 = Fire(256, 256, 32)
        self.fire6 = Fire(256, 384, 48)
        self.fire7 = Fire(384, 384, 48)
        self.fire8 = Fire(384, 512, 64)
        self.fire9 = Fire(512, 512, 64)

        self.conv10 = nn.Conv2d(512, class_num, 1)
        self.avg = nn.AdaptiveAvgPool2d(1)
        self.maxpool1 = nn.MaxPool2d(2, 2)
        self.maxpool2 = nn.MaxPool2d(2, 2)

        self.conv1 = nn.Conv2d(96, 128, 1)
        self.conv2 = nn.Conv2d(128, 256, 1)
        self.conv3 = nn.Conv2d(256, 384, 1)
        self.conv4 = nn.Conv2d(384, 512, 1)
    

    def forward(self, x):

        if self.version == 2:
            x = self.stem(x)

            f2 = self.fire2(x)
            f3 = self.fire3(f2) + f2
            f4 = self.fire4(f3)
            f4 = self.maxpool1(f4)

            f5 = self.fire5(f4) + f4
            f6 = self.fire6(f5)
            f7 = self.fire7(f6) + f6
            f8 = self.fire8(f7)
            f8 = self.maxpool2(f8)

            f9 = self.fire9(f8)
            c10 = self.conv10(f9)

            x = self.avg(c10)
            x = x.view(x.size(0), -1)

        elif self.version == 1:
            x = self.stem(x)
            x = self.fire2(x)
            x = self.fire3(x)
            x = self.fire4(x)
            x = self.maxpool1(x)

            x = self.fire5(x)
            x = self.fire6(x)
            x = self.fire7(x)
            x = self.fire8(x)
            x = self.maxpool2(x)

            x = self.fire9(x)
            x = self.conv10(x)
            x = self.avg(x)
            x = x.view(x.size(0), -1)

        elif self.version == 3:
            x = self.stem(x)
            f2 = self.fire2(x) + self.conv1(x)
            f3 = self.fire3(f2) + f2
            f4 = self.fire4(f3) + self.conv2(f3)
            f4 = self.maxpool1(f4)

            f5 = self.fire5(f4) + f4
            f6 = self.fire6(f5) + self.conv3(f5)
            f7 = self.fire7(f6) + f6
            f8 = self.fire8(f7) + self.conv4(f7)
            f8 = self.maxpool2(f8)

            f9 = self.fire9(f8) + f8
            c10 = self.conv10(f9)
            x = self.avg(c10)
            x = x.view(x.size(0), -1)
        return x


def squeezenet1(class_num=10):
    return SqueezeNet(class_num=class_num, version=1)

def squeezenet2(class_num=10):
    return SqueezeNet(class_num=class_num, version=2)

def squeezenet3(class_num=10):
    return SqueezeNet(class_num=class_num, version=3)
